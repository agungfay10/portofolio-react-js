import React from "react";
import "./index.css";
import "bootstrap/dist/css/bootstrap.min.css";
import Container from "react-bootstrap/Container";
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";
import Button from "react-bootstrap/Button";

export default class Styling extends React.Component {
  render() {
    return (
      <>
        <Navbar className="font" bg="dark" variant="dark">
          <Container>
            <Navbar.Brand href="#home">Agung Rifai</Navbar.Brand>
            <Nav className="me-auto">
              <Nav.Link href="#home">Biografi</Nav.Link>
              <Nav.Link href="#features">Contact</Nav.Link>
            </Nav>
          </Container>
        </Navbar>
        <header className="bg p-5">
          <div className="p-5 m-5 text-center">
            <h2 className="fst-italic">Fullstack Web Developer</h2>
            <h1>Agung Rifai</h1>
            <h1>Faedel</h1>
            <Button className="mt-5 border border-dark" variant="warning">Linkedin Profile</Button>
          </div>
        </header>
        <section>
          <Container className="my-2">
            <div className="text-center">
              <h1>My Bio</h1>
              <p className="fst-italic">
                Lorem, ipsum dolor sit amet consectetur adipisicing elit.
                Eligendi praesentium unde voluptatibus porro aut quo non,
                quaerat necessitatibus repellendus maiores dolorem? Quaerat,
                esse totam. Sunt, porro? Dolor laudantium reiciendis dolorem.
              </p>
            </div>
          </Container>
        </section>
      </>
    );
  }
}
